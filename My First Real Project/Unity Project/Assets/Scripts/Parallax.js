﻿#pragma strict

public var background : Transform;
public var midground : Transform;
public var backgroundParallaxScale : Vector3;
public var midgroundParallaxScale : Vector3;
private var lastPosition : Vector3;

function Start () {
    lastPosition = transform.position; 
	
}

function Update () {
    var move = transform.position - lastPosition;
    background.position += Vector3.Scale(move, backgroundParallaxScale);
    background.position += Vector3.Scale(move, midgroundParallaxScale);
    lastPosition = transform.position;
}
