﻿#pragma strict
public var maxHealth : float;
private var currentHealth : float;
public var healthSlider : UI.Slider;
public var SpawnPoint : Transform;

function Start () {
    currentHealth = maxHealth;
}

function Update () {
    
    if (transform.position.y < -7){
       
        
        currentHealth -= 40.0f;
        transform.position = SpawnPoint.position;

        if (currentHealth <= 0) {
            currentHealth = maxHealth;
            healthSlider.value = currentHealth;
           
        } else {
            healthSlider.value = currentHealth;
        }



    }
}
