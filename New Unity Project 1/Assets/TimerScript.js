﻿#pragma strict
 
var timer : float = 10.0;
private var anim : Animator;
public var canvas : GameObject;
private var restartTimer : float;       // Timer to count up to restarting the level
public var restartDelay : float = 5f;          // Time to wait before restarting the level



function Start () {
    anim = canvas.GetComponent(Animator);
}


function Update()
{

    timer -= Time.deltaTime;

    if(timer <= 0)
    {

        timer = 0;
        anim.SetTrigger("GameOver");

        restartTimer += Time.deltaTime;

        // .. if it reaches the restart delay...
        if(restartTimer >= restartDelay)
        {
            // .. then reload the currently loaded level.
            Application.LoadLevel(Application.loadedLevel);
        }
      
        //do some cool thing
    
    }
}
// Tints all GUI drawed elements with yellow.
function OnGUI()
{
    GUI.color = Color.yellow;
    GUI.Box(new Rect(325, 20, 50, 20), "" + timer.ToString("0"));
}
