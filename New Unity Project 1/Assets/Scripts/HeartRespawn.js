﻿#pragma strict


public var respawnTimer : float;
public var delayTime : float; 
public var objectPrefab : Transform; 
public var enemyToSpawn : Transform;

private function Respawn() {
    var enemy : GameObject = GameObject.Instantiate(enemyToSpawn.gameObject, transform.position, transform.rotation);
    enemy.gameObject.tag == "Health"; 
}


function Start () {
	
}

function OnCollisionEnter2D (collision : Collision2D) {

    if (collision.gameObject.tag == "Health"){   
        
        respawnTimer += Time.deltaTime;
        if(respawnTimer > delayTime){
            var newObject = Instantiate(objectPrefab, transform.position,
            transform.rotation);
            respawnTimer = 0.0;
            Respawn();
        }
    }
}

    

