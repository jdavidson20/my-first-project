﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Animating : MonoBehaviour {

    Animator anim;

	// Use this for initialization
	void Start () {
        anim = GetComponent<Animator>();
		
	}
	
	// Update is called once per frame
	void Update () {
        
        if (Input.GetKeyDown("right"))
            {

            anim.SetInteger("State", 1);
        }

        if (Input.GetKeyUp("right"))
        {

            anim.SetInteger("State", 0);
        }

        if (Input.GetKeyDown("left"))
        {

            anim.SetInteger("State", 1);
        }

        if (Input.GetKeyUp("left"))
        {

            anim.SetInteger("State", 0);
        }


    }
}
