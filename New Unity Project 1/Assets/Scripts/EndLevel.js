﻿#pragma strict
public var canvas : GameObject;
private var anim : Animator;


function Start () {
    anim = canvas.GetComponent(Animator);
}


function OnCollisionEnter2D(collision : Collision2D) {
    
    
    if ((collision.gameObject.tag == "Player")) {

        Debug.Log("End game");
        anim.SetTrigger("EndGame");

        yield WaitForSeconds (5);
        Application.LoadLevel(Application.loadedLevel);
        


    }
}