﻿#pragma strict

    // Reference to the player's health.
public var restartDelay : float = 5f;          // Time to wait before restarting the level
public var playerDamage : PlayerDamage;
public var healthSlider : UI.Slider;


private var anim : Animator;            // Reference to the animator component.
private var restartTimer : float;       // Timer to count up to restarting the level


function Start ()
{
    
   
    // Set up the reference.
    anim = GetComponent (Animator);
}


function Update ()
{
    // If the player has run out of health...
    if(playerDamage.currentHealth <= 0){

      
      
    
        // ... tell the animator the game is over.
        anim.SetTrigger ("GameOver");

        // .. increment a timer to count up to restarting.
        restartTimer += Time.deltaTime;

        // .. if it reaches the restart delay...
        if(restartTimer >= restartDelay)
        {
            // .. then reload the currently loaded level.
            Application.LoadLevel(Application.loadedLevel);
        }
       
	
    }
}

