﻿#pragma strict

 private var facingRight = true;
 private var jump = 0;
 public var moveForce : float = 365f;
 public var maxSpeed : float = 5f;
 public var jumpForce : float = 1000f;
 public var groundCheck : Transform;
 
private var grounded : boolean = false;
private var rb2d : Rigidbody2D;
private var doubleJump = false;
private var animator : Animator;



function Start() {
    rb2d = gameObject.GetComponent(Rigidbody2D) as Rigidbody2D;
    animator = GetComponent(Animator);
}

function Update() {


    // Check to see if the character is touching the ground
    grounded = Physics2D.Linecast(transform.position, groundCheck.position, 1 << LayerMask.NameToLayer("Ground"));
    if (grounded ){
        jump = 0;
    }

    // Jump only if character is on the ground ... you'll need to adjust this if you want double jump
    if (Input.GetButtonDown("Jump"))
    {
        jump = jump + 1;
        if (jump == 2) {
            doubleJump = true;
        }
    }
  
}


function FixedUpdate() {
	// Determine if player is moving character to the left or right
    var h = Input.GetAxis("Horizontal");
    animator.SetInteger("State", Mathf.Abs(h));

    // Apply force to move character in desired direction, not exceeding the max speed
    if (h * rb2d.velocity.x < maxSpeed)
        rb2d.AddForce(Vector2.right * h * moveForce);

    if (Mathf.Abs (rb2d.velocity.x) > maxSpeed)
        rb2d.velocity = new Vector2(Mathf.Sign (rb2d.velocity.x) * maxSpeed, rb2d.velocity.y);

    // Flip character sprite if switching direction
    if (h > 0 && !facingRight)
        Flip ();
    else if (h < 0 && facingRight)
        Flip ();

    // Add force in the y-direction if jumping ... you'll need to adjust if you want a double-jump
    if (jump > 0 && grounded)
    {
        animator.SetTrigger ("Jump");
        rb2d.AddForce(new Vector2(0f, jumpForce));
    } else if (doubleJump) {
        animator.SetTrigger ("Jump");
        rb2d.AddForce(new Vector2(0f, jumpForce));
        doubleJump = false;
    }

    animator.SetBool ("Ground", grounded);

   
}

function Flip() {
    facingRight = !facingRight;
    var theScale = transform.localScale;
    theScale.x *= -1;
    transform.localScale = theScale;
}

function OnCollisionEnter2D(other : Collision2D){

    if (other.gameObject.tag == "Enemy"){
        var direction = 1;
        var h = Input.GetAxis("Horizontal");
        if (facingRight && h > 0) {
            direction = -1;
        }
        if (!facingRight && h < 1) {
            direction = 1;
        }
        rb2d.AddForce(new Vector2(1000.0f * direction , 0));

    }
    

}
