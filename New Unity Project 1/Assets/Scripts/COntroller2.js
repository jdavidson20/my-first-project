﻿#pragma strict

public var speed : float;
public var gravity : float;
public var jumpSpeed : float;

private var verticalSpeed : float;
private var grounded : boolean;

private var animator : Animator;

function Start () {
    grounded = false;

    // We ask for the Animator component attached to our same
    // GameObject and store it in the animator var
    animator = GetComponent(Animator);
}

function Update () {
    var delta = Input.GetAxis ("Horizontal") * speed;
    transform.position.x += delta * Time.deltaTime;

    // Set the Speed value based on delta and 
    // use this to animate walking.
    animator.SetFloat ("Speed", Mathf.Abs (delta));

    if (grounded) {
        if (Input.GetButton ("Jump")) {
            animator.SetTrigger ("Jump");
            grounded = false;
            verticalSpeed = jumpSpeed;
        }
    } else {
        verticalSpeed -= gravity * Time.deltaTime;
        transform.position.y += verticalSpeed * Time.deltaTime;
    }
        
    // If we're not grounded we're probably falling...
    animator.SetBool ("Grounded", grounded);

    // We can use this to decide whether we're moving upward or
    // downward, could be used to animate jump vs fall
    animator.SetFloat ("VerticalSpeed", verticalSpeed);
}

function OnCollisionStay2D (other : Collision2D)
    {
        verticalSpeed = 0;
        grounded = true;
    }

    function OnCollisionExit2D (other : Collision2D)
        {
            grounded = false;
        }
