﻿#pragma strict

public var speed : float;
public var gravity : float;
public var jumpSpeed: float;

private var verticalSpeed: float;
private var grounded: boolean;

function Start () {
grounded = false;
}

function Update () {
transform.position.x += Input.GetAxis("Horizontal") * speed * 
Time.deltaTime;

if (grounded) {
if (Input.GetButton("Jump")) {
grounded = false;
verticalSpeed = jumpSpeed;}
} else {
verticalSpeed -= gravity * Time.deltaTime;
transform.position.y += verticalSpeed * Time.deltaTime;}
}

function OnCollisionStay2D (other : Collision2D){
verticalSpeed = 0;
grounded = true;
}

function OnCollisionExit2D (other : Collision2D){
grounded = false;
}
