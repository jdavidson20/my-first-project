﻿#pragma strict

public var maxHealth : float;
public var currentHealth : float;
public var healthSlider : UI.Slider;
public var spawnPoint : Transform;
public var bloodPrefab : Transform;
private var anim : Animator; 
public var Canvas : GameObject;
private var CanvasAnim : Animator;


function Start (){
    currentHealth = maxHealth;
    anim = GetComponent (Animator);
    CanvasAnim = Canvas.GetComponent(Animator);

}

function Update() {
    if (transform.position.y < -7){
       
        
        currentHealth = 0;
            
            
        //yield WaitForSeconds (5);
        //Application.LoadLevel(Application.loadedLevel);



    }
}


function OnCollisionEnter2D (collision : Collision2D) {
	if (collision.gameObject.tag == "Enemy") {
		if (bloodPrefab != null) {
			
		   Instantiate (bloodPrefab, collision.contacts[0].point, Quaternion.identity); 
		   currentHealth -= 20.0f;
		
		}
		
		if (currentHealth <= 0) {
			
			healthSlider.value = currentHealth;
			
		    
		    //transform.position = spawnPoint.position;
			
			

		} 
		
        else {
			healthSlider.value = currentHealth;
		}
	
			
	
		
	}

	if (collision.gameObject.tag == "Health") {

	    currentHealth += 40.0f;
	    healthSlider.value = currentHealth;
	    if (currentHealth > maxHealth) {


	        currentHealth = maxHealth;

	    }

	    Destroy(collision.gameObject);

	}

}

    
   